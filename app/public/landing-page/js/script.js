// Ripple intialization
$(function() {
    var ink, d, x, y;
    $(".ripplelink").click(function(e) {
        if ($(this).find(".ink").length === 0) {
            $(this).prepend("<span class='ink'></span>");
        }

        ink = $(this).find(".ink");
        ink.removeClass("animate");

        if (!ink.height() && !ink.width()) {
            d = Math.max($(this).outerWidth(), $(this).outerHeight());
            ink.css({
                height: d,
                width: d
            });
        }

        x = e.pageX - $(this).offset().left - ink.width() / 2;
        y = e.pageY - $(this).offset().top - ink.height() / 2;

        ink.css({
            top: y + 'px',
            left: x + 'px'
        }).addClass("animate");
    });
});

// Open Initalizations
jQuery(document).ready(function($) {
    $('.learn-more').click(function() {
        $('html,body').animate({
            scrollTop: $(".followup").offset().top
        });
    });

    // hover hiders
    $(".intro-l, .intro-r").on("mouseenter", function(){
      $(".learn-more").hide();
    });

    $(".intro-l, .intro-r").on("mouseleave", function(){
      $(".learn-more").show();
    });

    // Subscribe modal
    function IsEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }
    $("#csm-form").submit(function(e) {
      if (IsEmail($("#inputEmail3").val())) {
          var t = window.location.origin + "/api/v1/signup?signup=true";
          e.preventDefault();
          $.ajax({
              type: "POST",
              url: t,
              data: $("#csm-form").serialize(),
              success: function(e) {
                $(".tobehide").hide();
                $(".btn-danger").hide();
                $(".tobeText").text("Thankyou for Subscribing with us, we'll let you know as soon as we launch!" );
              }
          })
        }
        else{
          e.preventDefault();
        }
    });
    // $.browser.safari = ($.browser.webkit && !(/chrome/.test(navigator.userAgent.toLowerCase())));
    // if ($.browser.safari) {

    // }
    // initalize responsive owl
    $("#owl, #owl-l, #owl-live").owlCarousel({
        // Most important owl features
        items : 1,
        singleItem : true,
        itemsScaleUp : false,
     
        //Basic Speeds
        slideSpeed : 200,
        paginationSpeed : 800,
        rewindSpeed : 1000,
     
        //Autoplay
        autoPlay : true,
        stopOnHover : true,
     
        // Navigation
        navigation : true,
        navigationText : ["Previous","Next"],
        rewindNav : true,
        scrollPerPage : false,
     
        //Pagination
        pagination : true,
        paginationNumbers: false,
     
        // Responsive 
        responsive: true,
        responsiveRefreshRate : 200,
        responsiveBaseWidth: window,
        touchDrag: true
    });
    
    // Initalize smooth scrollTo
    smoothScroll.init({
        speed: 500, // Integer. How fast to complete the scroll in milliseconds
        easing: 'easeInOutCubic', // Easing pattern to use
        updateURL: false, // Boolean. Whether or not to update the URL with the anchor hash on scroll
        offset: 0
    });
    
    var timer;
    // Do a debounce
/*    $('#splitlayout').bind('mousewheel', function(e){
        clearTimeout(timer);
        if(e.originalEvent.wheelDelta /120 > 0) {
        }
        else{
        timer = setTimeout(function(){
          if ($('#sudo-bind').visible(true, true)) {
            $('html,body').animate({
                scrollTop: $(".followup").offset().top
            });
          }
        }, 100);
      }
    });
*/   // Initalize Google Maps
    function initialize() {
      var mapCanvas = document.getElementById('map-canvas');
      var mapOptions = {
        center: new google.maps.LatLng(28.549174, 77.215837),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      var map = new google.maps.Map(mapCanvas, mapOptions)
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    // Word rotator
    var quotes = $(".rw-words");
    var quoteIndex = -1;

    function showNextQuote() {
      ++quoteIndex;
      quotes.eq(quoteIndex % quotes.length)
          .fadeIn(300)
          .delay(1000)
          .fadeOut(300, showNextQuote);
    }

    showNextQuote();

    // sholders be window height
    var bodyheight = $("body").height();
    $(".sholder").height(bodyheight);
    $(".sholder").css("max-height", bodyheight);
    
    // Responsive owl
    $("#owl, #owl-l").height(bodyheight);

    // sholder pagination
    $(".leftnav a").click(function() {
        $(".leftnav a").removeClass("pagActive");
        $(this).addClass("pagActive");
    });
    $(".rightnav a").click(function() {
        $(".rightnav a").removeClass("pagActive");
        $(this).addClass("pagActive");
    });

    // Hide Header on on scroll down
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('header').outerHeight();
    $(window).scroll(function(event) {
        didScroll = true;
    });
    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
      var st = $(this).scrollTop();
      if (Math.abs(lastScrollTop - st) <= delta)
          return;
      if (st > lastScrollTop && st > navbarHeight) {
          // Scroll Down
          $('header').removeClass('nav-down').addClass('nav-up');
      } else {
          // Scroll Up
          if (st + $(window).height() < $(document).height()) {
              $('header').removeClass('nav-up').addClass('nav-down');
          }
      }
      lastScrollTop = st;
    }

/*    $('.intro-content').on("click", function() {
/*        $('.side').css("position", "fixed");
        $('.title-text').css("position", "fixed");
        $('.followup').hide();
        $('.footer').hide();
        $('#about').hide();
    });
*/
/*    $('.intro-l').click(function() {
        $('.download').css("left", "13%");
        if(!$(".leftnav a").hasClass("pagActive")){
          $("#m1").addClass("pagActive")
          $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-1.png')");
          $(".left-mck").css("background-size", "28em 28em");
          $(".left-mck").css("background-repeat", "no-repeat");
          setTimeout(function(){ 
            $('body').animate({
                scrollTop: 0
            });
          }, 300);
        }
    });
    $('.intro-r').click(function() {
        $('.gplus').css("left", "67%");
        if(!$(".rightnav a").hasClass("pagActive")){
          $("#d1").addClass("pagActive")
          $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-1.png')");
          $(".right-mck").css("background-size", "28em 28em");
          $(".right-mck").css("background-repeat", "no-repeat"); 
          setTimeout(function(){ 
            $('body').animate({
              scrollTop: 0
            }); 
          }, 300);
        }
    });
*//*    $('.back').on("click", function() {
        $('.download').css("left", "37%");
        $('.gplus').css("left", "36%");
        $("#live-events").fadeIn();
        $('.followup').show();
        $('.footer').show();
        $('#about').show();
        setTimeout(function() {
            // $('.side').css("position", "absolute");
            // $('.title-text').css("position", "absolute");
            $("body").css("overflow", "visible");
        }, 800);
        $(".leftnav a").removeClass("pagActive");
        $(".rightnav a").removeClass("pagActive");
    });*/
});

var timer;

// Scroll function
var scrollHandle = function (){
  
  // Scroll Events
/*  $("html, body").bind({
      'mousewheel DOMMouseScroll onmousewheel scroll': function(e) {
        if (e.target.id == 'el') return;
        e.preventDefault();
        e.stopPropagation();
        
        clearTimeout(timer);
        
        //Determine Direction
        if (e.originalEvent.wheelDelta && e.originalEvent.wheelDelta >= 0) {
          //Up
          timer = setTimeout(function(){
            if ($('#d-pitch5 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop:  $('body').height() * 3
                });
                $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-4.png')");
                $(".right-mck").css("background-size", "28em 28em");
                $(".right-mck").css("background-repeat", "no-repeat");
                $(".rightnav a").removeClass("pagActive");
                $("#d4").addClass("pagActive");
            } else if ($('#d-pitch4 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop:  $('body').height() * 2
                });
                $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-3.png')");
                $(".right-mck").css("background-size", "28em 28em");
                $(".right-mck").css("background-repeat", "no-repeat");
                $(".rightnav a").removeClass("pagActive");
                $("#d3").addClass("pagActive");
            } else if ($('#d-pitch3 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop:  $('body').height()
                });
                $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-2.png')");
                $(".right-mck").css("background-size", "28em 28em");
                $(".right-mck").css("background-repeat", "no-repeat");
                $(".rightnav a").removeClass("pagActive");
                $("#d2").addClass("pagActive");
            } else if ($('#d-pitch2 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop: 0
                });
                $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-1.png')");
                $(".right-mck").css("background-size", "28em 28em");
                $(".right-mck").css("background-repeat", "no-repeat"); 
                $(".rightnav a").removeClass("pagActive");
                $("#d1").addClass("pagActive");
            } else if ($('#m-pitch3 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop:  $('body').height()
                });
                $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-2.png')");
                $(".left-mck").css("background-size", "28em 28em");
                $(".left-mck").css("background-repeat", "no-repeat");
                $(".leftnav a").removeClass("pagActive");
                $("#m2").addClass("pagActive");
            } else if ($('#m-pitch2 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop:  0
                });
                $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-1.png')");
                $(".left-mck").css("background-size", "28em 28em");
                $(".left-mck").css("background-repeat", "no-repeat");
                $(".leftnav a").removeClass("pagActive");
                $("#m1").addClass("pagActive");
            }
          }, 100);
        } else {
          timer = setTimeout(function(){
            //Down
            if ($('#d-pitch1 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop:  $('body').height()
                });
                $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-2.png')");
                $(".right-mck").css("background-size", "28em 28em");
                $(".right-mck").css("background-repeat", "no-repeat");
                $(".rightnav a").removeClass("pagActive");
                $("#d2").addClass("pagActive");
            } else if ($('#d-pitch2 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop:  $('body').height() * 2
                });
                $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-3.png')");
                $(".right-mck").css("background-size", "28em 28em");
                $(".right-mck").css("background-repeat", "no-repeat");
                $(".rightnav a").removeClass("pagActive");
                $("#d3").addClass("pagActive");
            } else if ($('#d-pitch3 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop:  $('body').height() * 3
                });
                $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-4.png')");
                $(".right-mck").css("background-size", "28em 28em");
                $(".right-mck").css("background-repeat", "no-repeat");
                $(".rightnav a").removeClass("pagActive");
                $("#d4").addClass("pagActive");
            } else if ($('#d-pitch4 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop:  $('body').height() * 4
                });
                $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-5.png')");
                $(".right-mck").css("background-size", "28em 28em");
                $(".right-mck").css("background-repeat", "no-repeat");
                $(".rightnav a").removeClass("pagActive");
                $("#d5").addClass("pagActive");
            } else if ($('#m-pitch1 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop:  $('body').height()
                });
                $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-2.png')");
                $(".left-mck").css("background-size", "28em 28em");
                $(".left-mck").css("background-repeat", "no-repeat");
                $(".leftnav a").removeClass("pagActive");
                $("#m2").addClass("pagActive");
            } else if ($('#m-pitch2 .abs-pos').visible(true, true)) {
                $('body').animate({
                    scrollTop:  $('body').height() * 2
                });
                $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-3.png')");
                $(".left-mck").css("background-size", "28em 28em");
                $(".left-mck").css("background-repeat", "no-repeat");
                $(".leftnav a").removeClass("pagActive");
                $("#m3").addClass("pagActive");
            }
          }, 100);
        }
      }
  });*/
/*
  $(document).keyup(function(e) {

    clearTimeout(timer);
    switch (e.which) {
      case 38: // up
        timer = setTimeout(function(){
          if ($('#d-pitch5 .abs-pos').visible(true, true)) {
              $('body').animate({
                    scrollTop:  $('body').height() * 3
              });
              $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-4.png')");
              $(".right-mck").css("background-size", "28em 28em");
              $(".right-mck").css("background-repeat", "no-repeat");
              $(".rightnav a").removeClass("pagActive");
              $("#d4").addClass("pagActive");
          } 
          else if ($('#d-pitch4 .abs-pos').visible(true, true)) {
              $('body').animate({
                    scrollTop:  $('body').height() * 2
              });
              $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-3.png')");
              $(".right-mck").css("background-size", "28em 28em");
              $(".right-mck").css("background-repeat", "no-repeat");
              $(".rightnav a").removeClass("pagActive");
              $("#d3").addClass("pagActive");
          }
          else if ($('#d-pitch3 .abs-pos').visible(true, true)) {
              $('body').animate({
                    scrollTop:  $('body').height()
              });
              $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-2.png')");
              $(".right-mck").css("background-size", "28em 28em");
              $(".right-mck").css("background-repeat", "no-repeat");
              $(".rightnav a").removeClass("pagActive");
              $("#d2").addClass("pagActive");
          } else if ($('#d-pitch2 .abs-pos').visible(true, true)) {
              $('body').animate({
                    scrollTop: 0
              });
              $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-1.png')");
              $(".right-mck").css("background-size", "28em 28em");
              $(".right-mck").css("background-repeat", "no-repeat"); 
              $(".rightnav a").removeClass("pagActive");
              $("#d1").addClass("pagActive");
          } else if ($('#m-pitch3 .abs-pos').visible(true, true)) {
              $('body').animate({
                    scrollTop:  $('body').height()
              });
              $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-2.png')");
              $(".left-mck").css("background-size", "28em 28em");
              $(".left-mck").css("background-repeat", "no-repeat");
              $(".leftnav a").removeClass("pagActive");
              $("#m2").addClass("pagActive");
          } else if ($('#m-pitch2 .abs-pos').visible(true, true)) {
              $('body').animate({
                    scrollTop: 0
              });
              $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-1.png')");
              $(".left-mck").css("background-size", "28em 28em");
              $(".left-mck").css("background-repeat", "no-repeat");
              $(".leftnav a").removeClass("pagActive");
              $("#m1").addClass("pagActive");
          }
        }, 100);
        break;
      case 40: // down
        timer = setTimeout(function(){
          if ($('#d-pitch1 .abs-pos').visible(true, true)) {
              $('body').animate({
                  scrollTop:  $('body').height()
              });
              $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-2.png')");
              $(".right-mck").css("background-size", "28em 28em");
              $(".right-mck").css("background-repeat", "no-repeat");
              $(".rightnav a").removeClass("pagActive");
              $("#d2").addClass("pagActive");
          } else if ($('#d-pitch2 .abs-pos').visible(true, true)) {
              $('body').animate({
                    scrollTop:  $('body').height() * 2
              });
              $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-3.png')");
              $(".right-mck").css("background-size", "28em 28em");
              $(".right-mck").css("background-repeat", "no-repeat");
              $(".rightnav a").removeClass("pagActive");
              $("#d3").addClass("pagActive");
          } else if ($('#d-pitch3 .abs-pos').visible(true, true)) {
              $('body').animate({
                    scrollTop:  $('body').height() * 3
              });
              $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-4.png')");
              $(".right-mck").css("background-size", "28em 28em");
              $(".right-mck").css("background-repeat", "no-repeat");
              $(".rightnav a").removeClass("pagActive");
              $("#d4").addClass("pagActive");
          } else if ($('#d-pitch4 .abs-pos').visible(true, true)) {
              $('body').animate({
                    scrollTop:  $('body').height() * 4
              });
              $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-5.png')");
              $(".right-mck").css("background-size", "28em 28em");
              $(".right-mck").css("background-repeat", "no-repeat");
              $(".rightnav a").removeClass("pagActive");
              $("#d5").addClass("pagActive");
          } else if ($('#m-pitch1 .abs-pos').visible(true, true)) {
              $('body').animate({
                    scrollTop:  $('body').height()
              });
              $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-2.png')");
              $(".left-mck").css("background-size", "28em 28em");
              $(".left-mck").css("background-repeat", "no-repeat");
              $(".leftnav a").removeClass("pagActive");
              $("#m2").addClass("pagActive");
          } else if ($('#m-pitch2 .abs-pos').visible(true, true)) {
              $('body').animate({
                    scrollTop:  $('body').height() * 2
              });
              $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-3.png')");
              $(".left-mck").css("background-size", "28em 28em");
              $(".left-mck").css("background-repeat", "no-repeat");
              $(".leftnav a").removeClass("pagActive");
              $("#m3").addClass("pagActive");
          }
        }, 100);
        break;
      default:
        return; // exit this handler for other keys
    }
  });*/
}

// Handel Page scrolling 
$('.right-mck, .left-mck').click(function() {
    // overflow hack
    $("body").css("overflow", "hidden");
    $("header").addClass("navFix");

    $("#live-events").fadeOut();

    // sudo hack
    $("#sudo-bind").hide();

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
      // some code..
      $("html, body").unbind('mousewheel DOMMouseScroll onmousewheel scroll');
    }
    else{
      scrollHandle();
    }
});
/*
$('.back').on("click", function(e) {
    $("header").removeClass("navFix");
    $("html, body").unbind('mousewheel DOMMouseScroll onmousewheel scroll');
    $(".right-mck").css("background-size", "34em 34em");   
    $("#sudo-bind").show();
});
/*
// Nav clicks
$("#d1").click(function(){
  $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-1.png')");
  $(".right-mck").css("background-size", "28em 28em");
  $(".right-mck").css("background-repeat", "no-repeat"); 
  $(".rightnav a").removeClass("pagActive");
  $("#d1").addClass("pagActive");
});
$("#d2").click(function(){
  $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-2.png')");
  $(".right-mck").css("background-size", "28em 28em");
  $(".right-mck").css("background-repeat", "no-repeat"); 
  $(".rightnav a").removeClass("pagActive");
  $("#d2").addClass("pagActive");
});
$("#d3").click(function(){
  $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-3.png')");
  $(".right-mck").css("background-size", "28em 28em");
  $(".right-mck").css("background-repeat", "no-repeat"); 
  $(".rightnav a").removeClass("pagActive");
  $("#d3").addClass("pagActive");
});
$("#d4").click(function(){
  $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-4.png')");
  $(".right-mck").css("background-size", "28em 28em");
  $(".right-mck").css("background-repeat", "no-repeat"); 
  $(".rightnav a").removeClass("pagActive");
  $("#d4").addClass("pagActive");
});
$("#d5").click(function(){
  $(".right-mck").css("background", "url('../public/landing-page/img/Desktop_mockup/live-streaming-desktop-5.png')");
  $(".right-mck").css("background-size", "28em 28em");
  $(".right-mck").css("background-repeat", "no-repeat"); 
  $(".rightnav a").removeClass("pagActive");
  $("#d5").addClass("pagActive");
});
$("#m1").click(function(){
  $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-1.png')");
  $(".left-mck").css("background-size", "28em 28em");
  $(".left-mck").css("background-repeat", "no-repeat");
  $(".leftnav a").removeClass("pagActive");
  $("#m1").addClass("pagActive");
});
$("#m2").click(function(){
  $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-2.png')");
  $(".left-mck").css("background-size", "28em 28em");
  $(".left-mck").css("background-repeat", "no-repeat");
  $(".leftnav a").removeClass("pagActive");
  $("#m2").addClass("pagActive");
});
$("#m3").click(function(){
  $(".left-mck").css("background", "url('../public/landing-page/img/Mobile_mockup/live-streaming-mobile-3.png')");
  $(".left-mck").css("background-size", "28em 28em");
  $(".left-mck").css("background-repeat", "no-repeat");
  $(".leftnav a").removeClass("pagActive");
  $("#m3").addClass("pagActive");
});
*/

// Form Submit
$( "#queryForm" ).submit(function( event ) {
  event.preventDefault();

  var $form = $( this ),
    name          = $form.find( "input[name='name']" ).val(),
    email         = $form.find( "input[name='email']" ).val(),
    message       = $('#message').val(),
    url           = $form.attr( "action" );

  var posting = $.post( url, {name: name, email:email, message: message} );
  
  posting.done(function( data ) {
    if(data.error) {
      console.log(data)
    }
    else {
      $('#name').val("")
      $('#email').val("")
      $('#message').val("")
      $("#queryForm").hide();
      $("#queryFormDone").fadeIn();
    }
  })

  posting.fail( function(xhr, textStatus, errorThrown) {
    console.log('AAAAAAAAAAAAA')  
  })
});



/* Hi i am achin and this is my code */
var plugins = {
	slider : {
		containers : {},
		slideit : function(container)
		{
			var obj = this;
			var side = "";
			var sources = {
				left : [
					'../public/landing-page/img/Mobile_mockup/live-streaming-mobile-1.png',
					'../public/landing-page/img/Mobile_mockup/live-streaming-mobile-2.png',
					'../public/landing-page/img/Mobile_mockup/live-streaming-mobile-3.png',
				],
				right : [
					'../public/landing-page/img/Desktop_mockup/live-streaming-desktop-1.png',
					'../public/landing-page/img/Desktop_mockup/live-streaming-desktop-2.png',
					'../public/landing-page/img/Desktop_mockup/live-streaming-desktop-3.png',
					'../public/landing-page/img/Desktop_mockup/live-streaming-desktop-4.png',
					'../public/landing-page/img/Desktop_mockup/live-streaming-desktop-5.png'
				],
			}
			if($(".open-left").length)
			{
				side = "left";
			}
			else if($(".open-right").length)
			{
				side = "right";
			}
			$("#"+container).parents(".slide").siblings(".slide_buttons").find("li").removeClass("active");
			$("#"+container).parents(".slide").siblings(".slide_buttons").find("li:nth-child("+(obj.containers[container].slide_no+1)+")").addClass("active");
			if($("#"+container).parents(".slide").hasClass("vertical"))
			{
				var height = $("#"+container).find(".slide_element").outerHeight(),
				slide_height = 0;
				if($("#"+container.id).find(".slide_element").find(".slide_element_child").length)
				{
					var slide_child = $("#"+container).find(".slide_element").find(".slide_element_child");
					slide_height = slide_child.outerHeight()+parseFloat(slide_child.css("margin-top").replace("px", ""))+parseFloat(slide_child.css("margin-bottom").replace("px", ""));
				}
				shift = obj.containers[container].next_child?((height*(obj.containers[container].slide_no+1))+(slide_height*obj.containers[container].child_no)):(height*(obj.containers[container].slide_no+1));
				$("#"+container).animate({'bottom':shift+"px"}, "slow");
				$("."+side+"-mck").css("background", "url('"+sources[side][obj.containers[container].slide_no]+"') no-repeat");
				for(var b = 1; b <= obj.containers[container].slides; b++)
				{
				if(b == obj.containers[container].slide_no){ $("#slide_button"+b).css({"background":"#999", "border":"1px solid #dedede"}); }
				else{ $("#slide_button"+b).css({"background":"#f0f0f0", "border":"1px solid #999"}); }
				}

				if((obj.containers[container].slide_no == obj.containers[container].slides - 2 && obj.containers[container].next_child && obj.containers[container].child_no == -1) || (obj.containers[container].slide_no == obj.containers[container].slides - 3 && obj.containers[container].child_no > 0 && obj.containers[container].child_no == $(".slide_element:nth-child("+(obj.containers[container].slide_no+2)+")").find(".slide_element_child").length))
				{
				$("#slide_button1").css({"background":"#999", "border":"1px solid #dedede"});
				$("#"+container).animate({'bottom' : (height)+"px"}, 1);
				}
				else if(obj.containers[container].slide_no == -1)
				{
				$("#slide_button"+obj.containers[container].slides).css({"background":"#999", "border":"1px solid #dedede"});
				$("#"+container).animate({'bottom' : (height + (slide_height * obj.containers[container].slides))+"px"}, 1);
				}
			}
			else
			{
				var width = $("#"+container).find(".slide_element").outerWidth(),
				slide_width = 0;
				if($("#"+container).find(".slide_element").find(".slide_element_child").length)
				{
					var slide_child = $("#"+container).find(".slide_element").find(".slide_element_child");
					slide_width = slide_child.outerWidth()+parseFloat(slide_child.css("margin-left").replace("px", ""))+parseFloat(slide_child.css("margin-right").replace("px", ""));
				}
				shift = obj.containers[container].next_child?((width*(obj.containers[container].slide_no+1))+(slide_width*obj.containers[container].child_no)):(width*(obj.containers[container].slide_no+1));
				$("#"+container).animate({'right':shift+"px"}, "slow");
				for(var b = 1; b <= obj.containers[container].slides; b++)
				{
				if(b == obj.containers[container].slide_no){ $("#slide_button"+b).css({"background":"#999", "border":"1px solid #dedede"}); }
				else{ $("#slide_button"+b).css({"background":"#f0f0f0", "border":"1px solid #999"}); }
				}

				if((obj.containers[container].slide_no == obj.containers[container].slides - 2 && obj.containers[container].next_child && obj.containers[container].child_no == -1) || (obj.containers[container].slide_no == obj.containers[container].slides - 3 && obj.containers[container].child_no > 0 && obj.containers[container].child_no == $(".slide_element:nth-child("+(obj.containers[container].slide_no+2)+")").find(".slide_element_child").length))
				{
				$("#slide_button1").css({"background":"#999", "border":"1px solid #dedede"});
				$("#"+container).animate({'right' : (width)+"px"}, 1);
				}
				else if(obj.containers[container].slide_no == -1)
				{
				$("#slide_button"+obj.containers[container].slides).css({"background":"#999", "border":"1px solid #dedede"});
				$("#"+container).animate({'right' : (width + (slide_width * obj.containers[container].slides))+"px"}, 1);
				}
			}
		},
		start_interval : function(container)
		{
			var obj = this;
			obj.containers[container].slider = setInterval(function(){
				obj.slideit(container);
				current_slide_children = $("#"+container).find(".slide_element:nth-child("+(obj.containers[container].slide_no+2)+")").find(".slide_element_child");
				if(obj.containers[container].slide_children.length && ((obj.containers[container].slide_children.length == current_slide_children.length && obj.containers[container].child_no+1 < obj.containers[container].slide_children.length) || (obj.containers[container].slide_children.length > current_slide_children.length && obj.containers[container].child_no < current_slide_children.length)))
				{
					obj.containers[container].next_child = true; obj.containers[container].child_no++;
					/*if(child_no == $(".slide_element:nth-child("+(obj.slide_no+2)+")").find(".slide_element_child").length)*/
				}
				else
				{
					if(obj.containers[container].slide_children.length)
					{
						obj.containers[container].child_no = 0; obj.containers[container]./*slide_no++;*/
						obj.containers[container].next_child = false;
						if(obj.containers[container].slide_no == obj.containers[container].slides-2)
						{
							obj.containers[container].child_no = 1;
							obj.containers[container].slide_no = 0;
							obj.containers[container].next_child = true;
						}
					}
					else if(!obj.containers[container].slide_children.length)
					{
						obj.containers[container].child_no = -1; obj.containers[container]./*slide_no++;*/
						obj.containers[container].next_child = false;
						if(obj.containers[container].slide_no == obj.containers[container].slides-2)
						{
							obj.containers[container].next_child = true;
						}
						if(obj.containers[container].slide_no == obj.containers[container].slides-1)
						{
							obj.containers[container].slide_no = 1;
							obj.containers[container].next_child = false;
						}
					}
				}
			}, 4000);
		},
		slide_start : function(container, slides)
		{
			var obj = this;
			obj.containers[container.id].slides = parseInt(slides);
			var width = $("#"+container.id).find(".slide_element").width()/$("#"+container.id).width()*100;
			if($("#"+container.id).parents(".slide").hasClass("vertical"))
			{
				$("#"+container.id).css({height:(100*obj.containers[container.id].slides)+"%"});
				$("#"+container.id).find(".slide_element").css({height:(100/obj.containers[container.id].slides)+"%"});
			}
			else
			{
				if(width == 90)
				{
					$("#"+container.id).css({width:(100*obj.containers[container.id].slides)*$("#"+container.id).find(".slide_element:first-child").find(".slide_element_child").length+"%"});
					$("#"+container.id).find(".slide_element").css({width:(100/obj.containers[container.id].slides)+"%"});					
				}
				else
				{
					$("#"+container.id).css({width:(100*obj.containers[container.id].slides)+"%"});
					$("#"+container.id).find(".slide_element").css({width:(100/obj.containers[container.id].slides)+"%"});
				}
			}
			obj.containers[container.id].slide_children = $("#"+container.id).find(".slide_element:nth-child(2)").find(".slide_element_child"),
			obj.containers[container.id].child_no = obj.containers[container.id].slide_children.length?1:-1,
			obj.containers[container.id].next_child = obj.containers[container.id].slide_children.length?true:false,
			obj.containers[container.id].slide_no = obj.containers[container.id].slide_children.length?0:1;
			if(container.id != "left_slider" && container.id != "right_slider")
			{
				obj.start_interval(container.id);
			}

			$("#"+container.id).parents(".slide").siblings(".slide_buttons").find("li").click(function(){
				obj.containers[container.id].slide_no = $(this).index();
				obj.slideit(container.id);
/*				obj.containers[container.id].slide_no++;
				if(obj.containers[container.id].slide_no == obj.containers[container.id].slides){obj.containers[container.id].slide_no = 2;}
*/				clearInterval(obj.containers[container.id].slider);
/*				obj.containers[container.id].slide_no = $(this).index() + 1;
*/
				if(container.id != "left_slider" && container.id != "right_slider")
				{
					obj.start_interval(container.id);
				}
			});


			$("#"+container.id).find("#slide_left_arrow").click(function(){
			clearInterval(obj.containers[container.id].slider);
			obj.containers[container.id].slide_no = obj.containers[container.id].slide_no-2; if(obj.containers[container.id].slide_no == -1){obj.containers[container.id].slide_no = obj.containers[container.id].slides;}
			obj.slideit(container.id);
			obj.containers[container.id].slide_no++; if(obj.containers[container.id].slide_no == obj.containers[container.id].slides){obj.containers[container.id].slide_no = 0;}
			if(container.id != "left_slider" && container.id != "right_slider")
			{
				obj.start_interval(container.id);
			}
			});

			$("#"+container.id).find("#slide_right_arrow").click(function(){
			clearInterval(obj.containers[container.id].slider);
			if(obj.containers[container.id].slide_no == obj.containers[container.id].slides){obj.containers[container.id].slide_no = 2;}
			obj.slideit(container.id);
			obj.containers[container.id].slide_no++; if(obj.containers[container.id].slide_no == obj.containers[container.id].slides){obj.containers[container.id].slide_no = 2;}
			if(container.id != "left_slider" && container.id != "right_slider")
			{
				obj.start_interval(container.id);
			}
			});
		},
		onkeydown : function(evt)
		{
			var obj = this;
			evt = (evt) ? evt : document.event;
			var charcode = (evt.which) ? evt.which : evt.keyCode;
			var side = "";
			if($(".open-left").length)
			{
				side = "left";
			}
			else if($(".open-right").length)
			{
				side = "right";
			}
			if(side != "")
			{
				var container = $(".page-"+side).find(".slide_main").attr("id");
				clearTimeout(obj.containers[container].timer);
				if(charcode == 40)
				{
					obj.containers[container].timer = setTimeout(function(){
						clearInterval(obj.containers[container].slider);
						if(obj.containers[container].hasOwnProperty("event") && obj.containers[container].event == "prev")
						{
							obj.containers[container].slide_no++;
						}
						if(obj.containers[container].slide_no == 0)
						{
							obj.containers[container].slide_no = 1;
						}
						else if(obj.containers[container].slide_no >= obj.containers[container].slides - 2)
						{
							obj.containers[container].slide_no = obj.containers[container].slides - 3;
						}
						obj.slideit(container);
						obj.containers[container].slide_no++;
						if(obj.containers[container].slide_no >= (obj.containers[container].slides - 2)){obj.containers[container].slide_no = obj.containers[container].slides - 2;}
						if(container != "left_slider" && container != "right_slider")
						{
							obj.start_interval(container);
						}
						obj.containers[container].event = "next";
					}, 200);
				}
				if(charcode == 38)
				{
					obj.containers[container].timer = setTimeout(function(){
						if(obj.containers[container].hasOwnProperty("event") && obj.containers[container].event == "next")
						{
							obj.containers[container].slide_no-=2;
						}
						else
						{
							obj.containers[container].slide_no--;
						}
						if(obj.containers[container].slide_no < 1){obj.containers[container].slide_no = 0;}
						clearInterval(obj.containers[container].slider);
						obj.slideit(container);
						obj.containers[container].event = "prev";
						if(container != "left_slider" && container != "right_slider")
						{
							obj.start_interval(container);
						}
					}, 200);
				}
			}
		},
		onready : function()
		{
			var obj = this;
			$(".slide_main").each(function(){
//				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )
				if($(".home-left").css("opacity") == 1)
				{
					$(this).parents(".slide").removeClass("vertical");
				}
				container = $(this).attr("id");
				obj.containers[container] = {id : container};
				clearInterval(obj.containers[container].slider);
				var slides = $(this).find(".slide_element").length,
				slide_child = $(this).find(".slide_element:first-child").find(".slide_element_child"),
				slide_last_child = $(this).find(".slide_element:last-child").find(".slide_element_child");
				var append_slide_content = "<div class='slide_element last_element'>"+$(this).find(".slide_element:first-child").html()+"</div>";
				var prepend_slide_content = "<div class='slide_element first_element'>"+$(this).find(".slide_element:last-child").html()+"</div>";
				var last_element_length = $(this).find(".last_element").length;
				if(!last_element_length)
				{
					$(this).append(append_slide_content);
				}
				if(!$(this).find(".first_element").length)
				{
					$(this).prepend(prepend_slide_content);
				}
				slides = slides + 2;
				$(this).parents(".slide").siblings(".slide_buttons").find("li:nth-child(1)").addClass("active");
				obj.slide_start(obj.containers[container], slides);
				if(slide_child.length && !last_element_length)
				{
					if($(this).parents(".slide").hasClass("vertical"))
					{
						$(this).find(".slide_element.last_element").prev().css({"marginTop":-(($(this).find(".slide_element").outerHeight()-$(this).find(".slide_element_child").css("margin-bottom").replace("px", ""))*((slide_child.length-slide_last_child.length)/slide_child.length))+"px"});
					}
					else
					{
						$(this).find(".slide_element.last_element").prev().css({"marginRight":-(($(this).find(".slide_element").outerWidth()-$(this).find(".slide_element_child").css("margin-right").replace("px", ""))*((slide_child.length-slide_last_child.length)/slide_child.length))+"px"});
					}
				}
			});
			$(".slide").swipe( {
				swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
					var side = "";
					if($(".open-left").length)
					{
						side = "left";
					}
					else if($(".open-right").length)
					{
						side = "right";
					}
					if(side != "")
					{
						var container = $(".page-"+side).find(".slide_main").attr("id");
						clearTimeout(obj.containers[container].timer);
						if (direction == "right") {
							obj.containers[container].timer = setTimeout(function(){
								if(obj.containers[container].hasOwnProperty("event") && obj.containers[container].event == "next")
								{
									obj.containers[container].slide_no-=2;
								}
								else
								{
									obj.containers[container].slide_no--;
								}
								if(obj.containers[container].slide_no < 1){obj.containers[container].slide_no = 0;}
								clearInterval(obj.containers[container].slider);
								obj.slideit(container);
								obj.containers[container].event = "prev";
							}, 200);
						}
						else{
							obj.containers[container].timer = setTimeout(function(){
								if(obj.containers[container].hasOwnProperty("event") && obj.containers[container].event == "prev")
								{
									obj.containers[container].slide_no++;
								}
								if(obj.containers[container].slide_no == 0)
								{
									obj.containers[container].slide_no = 1;
								}
								else if(obj.containers[container].slide_no >= obj.containers[container].slides - 2)
								{
									obj.containers[container].slide_no = obj.containers[container].slides - 3;
								}
							obj.slideit(container);
							obj.containers[container].slide_no++;
							if(obj.containers[container].slide_no >= (obj.containers[container].slides - 2)){obj.containers[container].slide_no = obj.containers[container].slides - 2;}
							obj.containers[container].event = "next";
							}, 200);
						}
					}

				}
			});
			$(window).bind('mousewheel', function(event) {
				var side = "";
				if($(".open-left").length)
				{
					side = "left";
				}
				else if($(".open-right").length)
				{
					side = "right";
				}
				if(side != "")
				{
					var container = $(".page-"+side).find(".slide_main").attr("id");
					clearTimeout(obj.containers[container].timer);
					if (event.originalEvent.wheelDelta/120 >= 0) {
						obj.containers[container].timer = setTimeout(function(){
							if(obj.containers[container].hasOwnProperty("event") && obj.containers[container].event == "next")
							{
								obj.containers[container].slide_no-=2;
							}
							else
							{
								obj.containers[container].slide_no--;
							}
							if(obj.containers[container].slide_no < 1){obj.containers[container].slide_no = 0;}
							clearInterval(obj.containers[container].slider);
							obj.slideit(container);
							obj.containers[container].event = "prev";
						}, 200);
					}
					else{
						obj.containers[container].timer = setTimeout(function(){
							if(obj.containers[container].hasOwnProperty("event") && obj.containers[container].event == "prev")
							{
								obj.containers[container].slide_no++;
							}
							if(obj.containers[container].slide_no == 0)
							{
								obj.containers[container].slide_no = 1;
							}
							else if(obj.containers[container].slide_no >= obj.containers[container].slides - 2)
							{
								obj.containers[container].slide_no = obj.containers[container].slides - 3;
							}
						obj.slideit(container);
						obj.containers[container].slide_no++;
						if(obj.containers[container].slide_no >= (obj.containers[container].slides - 2)){obj.containers[container].slide_no = obj.containers[container].slides - 2;}
						obj.containers[container].event = "next";
						}, 200);
					}
				}
			});
		}
	},
};
$(document).ready(function(){
	$(document).click(function(e){
		if(($(".open-left").length && $(".page-left").has(e.target).length == 0 && !$(".page-left").is(e.target)) || ($(".open-right").length && $(".page-right").has(e.target).length == 0 && !$(".page-right").is(e.target)))
		{
			$("body").css("overflow", "visible");
			$("header").removeClass("navFix");
			$(".splitlayout").removeClass("open-left").removeClass("open-right");
      $('.followup').show();
      $('.footer').show();
      $('.press').show();
      $('#about').show();
		}
	});
	$(".left-mck").click(function(){
		if(!$(".splitlayout").hasClass("open-left"))
		{
			$(".splitlayout").addClass("open-left");
      $('.followup').hide();
      $('.footer').hide();
      $('.press').hide();
      $('#about').hide();
      $(window).scrollTop(0);
			return false;
		}
	});
	$(".right-mck").click(function(){
		if(!$(".splitlayout").hasClass("open-right"))
		{
			$(".splitlayout").addClass("open-right");
      $('.followup').hide();
      $('.footer').hide();
      $('.press').hide();
      $('#about').hide();
      $(window).scrollTop(0);
			return false;
		}
	});
	$.each(plugins, function(key, func){
		if($.isFunction(func.onready))
		{
			func.onready();
		}
	});
	$(document).unbind("keydown").keydown(function(evt){
		$.each(plugins, function(key, func){
			if($.isFunction(func.onkeydown))
			{
				func.onkeydown(evt);
			}
		});
	});
	$(window).scroll(function(){
		$.each(plugins, function(key, func){
			if($.isFunction(func.onscroll))
			{
				func.onscroll();
			}
		});
	});

});
