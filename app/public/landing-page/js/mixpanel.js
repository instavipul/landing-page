$("#intro-l").click(function() {
  mixpanel.track("B2C side clicked", {
    "id": "intro-l",
    "date": Date()
  });
});

$("#intro-r").click(function() {
  mixpanel.track("B2B side clicked", {
    "id": "intro-r",
    "date": Date()
  });
});

$("#download").click(function() {
  mixpanel.track("Get it on GooglePlay clicked", {
    "id": "download",
    "date": Date()
  });
});

$("#gplus").click(function() {
  mixpanel.track("Signin with google clicked", {
    "id": "gplus",
    "date": Date()
  });
});

$("#signup-beta").click(function() {
  mixpanel.track("Signed up for beta", {
    "id": "signup-beta",
    "date": Date()
  });
});

$("#vid-rg").click(function() {
  mixpanel.track("Partner with videographer clicked", {
    "id": "vid-rg",
    "date": Date()
  });
});